from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import LoginForm, RegistrationForm
from django.contrib.auth import login as auth_login,authenticate
from django.contrib.auth.models import User

# Create your views here.
def login(request):
    if request.method =='POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username=form.cleaned_data['username']
            password=form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponse('Login Successfully')
    else:
        form = LoginForm()
    context = {
        'form':form
    }
    return render(request, 'accounts/login.html', context)

def registration(request):
    if request.method == 'POST':
        rform = RegistrationForm(request.POST)
        if rform.is_valid():
            username=rform.cleaned_data['username']
            email = rform.cleaned_data['email']
            first_name=rform.cleaned_data['fname']
            last_name=rform.cleaned_data['lname']
            password1=rform.cleaned_data['password1']
            password2=rform.cleaned_data['password2']

            if password1 == password2 :
                if not User.objects.filter(username=username).exists():
                    user = User.objects.create_user(username=username, password=password2, first_name=first_name, last_name=last_name, email=email)
                    user.save()
                    HttpResponse('created succesfully')
    rform = RegistrationForm()
    context = {
        'form':rform
    }
    return render(request, 'accounts/signup.html', context)