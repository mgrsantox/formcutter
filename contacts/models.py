from django.db import models

# Create your models here.
class Contact(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    message = models.TextField(blank=True)
    add = models.CharField(max_length =300, default="Kathmandu")

    def __str__(self):
        return self.name
        