from django import forms
from .models import Contact
from django.forms import TextInput, Textarea, EmailInput


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'email', 'add', 'message']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control'}),
            'email': EmailInput(attrs={'class': 'form-control'}),
            'add': TextInput(attrs={'class': 'form-control'}),
            'message': Textarea(attrs={'class': 'form-control'}),
        }