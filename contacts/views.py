from django.shortcuts import render
from django.core.mail import send_mail
from .models import Contact
from .forms import ContactForm

# Create your views here.
def contact(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        address = request.POST['address']
        message = request.POST['message']

        conn = Contact(name=name, email=email, add=address, message=message)
        conn.save()

        send_mail(
            'message',
            message,
            'fromsome@email.com',
            ['toemail.com'],
            fail_silently= False
        )

    return render(request, 'contact.html')

def mcontact(request):
    form = ContactForm
    myform = ContactForm(request.POST)
    if myform.is_valid():
        name = myform.cleaned_data['name']
        email = myform.cleaned_data['email']
        address = myform.cleaned_data['add']
        message = myform.cleaned_data['message']
        fm=Contact(name=name, add=address, message=message,email=email)
        fm.save()
    context = {
        'form':form
    }
    return render(request, 'mcontact.html', context)